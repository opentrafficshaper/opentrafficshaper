# OpenTrafficShaper POE::Filter::TCStatistics TC stats filter
# OpenTrafficShaper webserver module: limits page
# Copyright (C) 2007-2023, AllWorldIT
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


##
# Code originally based on POE::Filter::HTTPD
##
# Filter::HTTPD Copyright 1998 Artur Bergman <artur@vogon.se>.
# Thanks go to Gisle Aas for his excellent HTTP::Daemon.	Some of the
# get code was copied out if, unfortunately HTTP::Daemon is not easily
# subclassed for POE because of the blocking nature.

# 2001-07-27 RCC: This filter will not support the newer get_one()
# interface.	It gets single things by default, and it does not
# support filter switching.	If someone absolutely needs to switch to
# and from HTTPD filters, they should submit their request as a patch.
##

package opentrafficshaper::POE::Filter::TCStatistics;

use warnings;
use strict;

use JSON qw(decode_json);
use POE::Filter;

use vars qw($VERSION @ISA);
# NOTE - Should be #.### (three decimal places)
$VERSION = '3.000';
@ISA = qw(POE::Filter);



# Class instantiation
sub new
{
	my $class = shift;

	# These are our internal properties
	my $self = { };
	# Build our class
	bless($self, $class);

	# And initialize
	$self->_reset();

	return $self;
}



# From the docs:
# get_one_start() accepts an array reference containing unprocessed stream chunks. The chunks are added to the filter's Internal
# buffer for parsing by get_one().
sub get_one_start
{
	my ($self, $stream) = @_;


	# Join all the blocks of data and add to our buffer
	$self->{'buffer'} .= join('',@{$stream});

	return $self;
}



# This is called to see if we can grab records/items
sub get_one
{
	my $self = shift;
	my @results = ();


	# If we have a empty buffer we can just return
	return [] if ($self->{'buffer'} eq '');
	chomp($self->{'buffer'});
	# NK: It seems we may not get the terminating ] at the end if the output is still busy?
	return [] if (substr($self->{'buffer'},-1) ne "]");

	# Try decode JSON payload
	my $items;
	eval {
		$items = decode_json($self->{'buffer'});
		$self->{'buffer'} = '';
		1;
	} or do {
		print(STDERR "FAILED TO DECODE JSON: >" . $self->{'buffer'} . "<\n");
		return [];
	};

	# Loop with each item and generate a current stat
	for my $item (@{$items}) {
		my $curstat = {};

		# Skip everything except HTB
		if ($item->{'class'} ne "htb") {
			continue;
		}

		# Split off the handle into the parent and child
		( $curstat->{'TCClassParent'}, $curstat->{'TCClassChild'} ) = split( /:/, $item->{'handle'} );
		# The rate and ceil is represented in bytes/s, so it needs to be multiplied by 8 and divided by 1000 to get Kbit/s
		$curstat->{'CIR'} = (int($item->{'rate'}) * 8) / 1000;
		$curstat->{'Limit'} = (int($item->{'ceil'}) * 8) / 1000;
		# If we have a prio, we need to add this too
		if (defined($item->{'prio'})) {
			$curstat->{'Priority'} = int($item->{'prio'});
		}
		# We should probably always have these
		$curstat->{'TotalBytes'} = int($item->{'stats'}->{'bytes'}) // 0;
		$curstat->{'TotalPackets'} = int($item->{'stats'}->{'packets'}) // 0;
		$curstat->{'TotalDropped'} = int($item->{'stats'}->{'drops'}) // 0;
		$curstat->{'TotalOverLimits'} = int($item->{'stats'}->{'overlimits'}) // 0;
		$curstat->{'QueueSize'} = int( $item->{'stats'}->{'backlog'} ) // 0;
		$curstat->{'QueueLen'} = int( $item->{'stats'}->{'qlen'} ) // 0;
		# These are HTB specific if stats are enabled
		$curstat->{'Rate'} = 0;
		if (defined($item->{'stats'}->{'rate'})) {
			$curstat->{'Rate'} = (int($item->{'stats'}->{'rate'}) * 8) / 1000;
		}
		$curstat->{'PPS'} = 0;
		if (defined($item->{'stats'}->{'pps'})) {
			$curstat->{'PPS'} = int($item->{'stats'}->{'pps'});
		}
		push(@results,$curstat);
	}

	return [ @results ];
}



# Function to push data to the socket
sub put
{
	my ($self, $data) = @_;

	my @results = [ $data ];

	return \@results;
}



#
# Internal functions
#

# Prepare for next request
sub _reset
{
	my $self = shift;


	# Reset our filter state
	$self->{'buffer'} = '';
}



# Get rate...
sub _getKNumber
{
	my $str = shift;

	my ($num,$multiplier) = ($str =~ /([0-9]+)([KMG])?/);

	# We only work in Kbit
	if (!defined($multiplier)) {
		$num /= 1000;
	} elsif ($multiplier eq "K") {
		# noop
	} elsif ($multiplier eq "M") {
		$num *= 1000;
	} elsif ($multiplier eq "G") {
		$num *= 1000000;
	}

	return int($num);
}



1;
