/*
 * AllWorldIT Data Sources for Flot
 * Copyright (c) 2013-2014, AllWorldIT
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 *
 */

/* Usage Example:
 *
 *	options = {
 *
 *		awitds: {
 *			sources: [
 *				{
 *					type: 'websocket',
 *					uri: 'ws://ots-devel:8088/statistics/graphdata',
 *					shared: true,
 *					// Websocket specific
 *					onconnect: [
 *						{ 'function': 'subscribe', args: ['tag1','pool=eth4,eth5:nigeltest'] }
 *					]
 *				},
 *				{
 *					type: 'ajax',
 *					url: "http://ots-devel:8088/statistics/jsondata?pool=tag1:eth4,eth5:nigeltest"
 *				}
 *			],
 *			xaxis: {
 *				'tag1': {
 *					'tx.cir': {
 *						label: 'TX Cir',
 *						maxCount: 1,
 *						maxTimespan: 300,
 *						overrideTimestamp: 1
 *					},
 *					'tx.limit': {
 *						label: 'TX Limit'
 *					},
 *					'tx.rate': {
 *						label: 'TX Rate'
 *					},
 *					'rx.cir': {
 *						label: 'RX Cir'
 *					},
 *					'rx.limit': {
 *						label: 'RX Limit'
 *					},
 *					'rx.rate': {
 *						label: 'RX Rate'
 *					}
 *				},
 *				'tag2': {
 *					'tx.rate': {
 *						label: 'Client2 Rate'
 *					}
 *				}
 *			},
 *
 *			yaxis: {
 *				'tag3': {
 *					'configmanager.totalpools': {
 *						label: 'Total Pools'
 *					}
 *				}
 *			}
 *	};
 */


(function ($) {
	// Options we can customize
	var options = {
		// Turn on debugging
		debug: true,
		// Default maximum number of items for each dataset to 0 (disabled)
		defaultMaxCount: 0,
		// Default maximum timespan to 0 (disabled)
		defaultMaxTimespan: 0
	};

	var WS_TICK_INTERVAL = 1000;
	var AXES = [ 'xaxis', 'yaxis' ];

	// Datasources to plot mapping, indexed by datasource ref
	var datasources = { };
	// Plot configuration, indexed by options.id , then has xaxis and yaxis
	var plots = { };

	var graphData = [ ];



	// Function to log messages
	function log_message(message)
	{
		if (typeof(console.log) !== 'undefined' && options.debug) {
			console.log(message);
		}
	}


	// Initialization
	function init(plot)
	{

		plot.hooks.processOptions.push(function (plot, options) {

			// We need datasources to continue
			if (typeof(options.awitds.sources) === 'undefined') {
				return;
			}

			// Loop with datasources
			for (var i = 0; i < options.awitds.sources.length; i++) {
				var datasource = options.awitds.sources[i];

				// Check type is present
				if (typeof(datasource.type) === 'undefined') {
					log_message("Datasource type not provided");
					continue;
				}

				// Datasource reference
				var ref;
				if (datasource.type == "websocket") {
					ref = _register_websocket(options,datasource);
				} else if (datasource.type == "ajax") {
					ref = _register_ajax(options,datasource);
				} else {
					log_message("Unknown datasource type '"+datasource.type+"'");
					continue;
				}

				// Add graph to datasource
				datasources[ref].plots[options.id] = plot;
			}

			// Add plot including its xaxis and yaxis config
			plots[options.id] = {
				handle: plot,
				loading: true,
				xaxis: options.awitds.xaxis,
				yaxis: options.awitds.yaxis
			};

			// Initialize graph data for the plot
			_init_graph_data(options.id);

			// Display loading image
			jQuery('#'+options.id).addClass('awitds-loader');
		});

	}


	/*
	 * Internal functions
	 */


	// Function to register a websocket
	function _register_websocket(options,datasource)
	{
		// Must have a uri
		if (typeof(datasource.uri) === 'undefined') {
			log_message('Websocket datasource has no "uri" attribute');
			return;
		}

		// This is our unique datasource, websocket name
		var wsname;
		if (typeof(datasource.shared) !== 'undefined' && datasource.shared) {
			wsname = datasource.uri;
		} else {
			wsname = options.id;
		}

		// Get websocket handle
		var websocket = _websocket_connect(wsname,datasource.uri);

		// Check if we need to send anything to the server
		if (typeof(datasource.onconnect) !== 'undefined') {
			// Loop with data to send
			for (var i = 0; i < datasource.onconnect.length; i++) {
				var msg = datasource.onconnect[i];

				// We need to give the message an ID, the ID is used for tracking return status
				msg.id = websocket.cmdid++;

				_websocket_send(wsname,msg);
			}
		}

		return wsname;
	}


	// WebSocket connect function
	function _websocket_connect(ref,uri)
	{

		// Check if socket with this ref exists
		if (typeof(datasources[ref]) === 'object') {
			return datasources[ref];
		}

		// Try connect
		try {
			// Create websocket and assign handlers
			var socket = window['MozWebSocket'] ? new MozWebSocket(uri) : new WebSocket(uri);
			// Pull in ref to all our events so we know which websocket it is
			socket.onopen = function() { _websocket_onopen(ref); };
			socket.onclose = function() { _websocket_onclose(ref); };
			socket.onerror = function(error) { _websocket_onerror(ref,error); };
			socket.onmessage = function(message) { _websocket_onmessage(ref,message); };

			// Inititalize datasources for this ref
			datasources[ref] = {
				// Socket handle
				handle: socket,
				// Command ID , we sequence commands to track return status
				cmdid: 1,
				// Queue of commands left to send
				cmdqueue: [ ],
				// General config, plots linked to this datasource
				plots: { }
			};

		} catch (e) {
			log_message('Failed to connect to "' + uri + '": ' + e);
			return 'Failed to connect to "' + uri + '": ' + e;
		}

		return datasources[ref];
	}


	// Send data to a websocket
	function _websocket_send(ref,data)
	{
		var websocket = datasources[ref];

		websocket.cmdqueue.push(data);
	}


	// Function to handle the open event
	function _websocket_onopen(ref)
	{
		var websocket = datasources[ref];

		log_message('WebSocket Open: '+ref);

		// Setup ticker
		_websocket_ontick(ref);
	}


	// Function to handle close event
	function _websocket_onclose(ref)
	{
		var websocket = datasources[ref];

		log_message('WebSocket Close: '+ref);

		// Remove websocket from list
		delete datasources[ref];
	}


	// Function to handle error events
	function _websocket_onerror(ref,error)
	{
		var websocket = datasources[ref];

		log_message('WebSocket Error "'+ref+'": '+error);
	}


	// Function to handle incoming message events
	function _websocket_onmessage(ref,message)
	{
		var websocket = datasources[ref];

		log_message('WebSocket Message "'+ref+'"');

		var response;
		try {
			response = JSON.parse(message.data);
		} catch (e) {
			log_message('WebSocket Message Exception: ' + e);
			// XXX - just return? display error
			return;
		}

		// Check result
		if (response.status == "success") {
			// Check if this is out of band data
			if (typeof(response.id) !== 'undefined' && response.id == -1) {
				log_message(" - Data: "+message.data);
				// Update plots with the data we just got
				_update_plots(websocket,response.data);

			} else {
				// If this is out of band data, check if its tagged
				log_message("Command success");
			}

		} else if (response.status == "error") {
			log_message('WebSocket Server Error: '+response.message);

		} else if (response.status == "fail") {
			log_message('WebSocket Server Fail: '+response.message);
		}

	}


	// Periodic tick on socket
	function _websocket_ontick(ref)
	{
		var websocket = datasources[ref];

		// We may of done a cleanup of this ref before ending up here
		if (typeof(websocket.cmdqueue) === 'undefined') {
			return;
		}

		// If we have pending commands, send one
		while (websocket.cmdqueue.length) {
			__websocket_send(ref,websocket.cmdqueue.shift());
		}

		// Setup ticker again
		setTimeout( function() { _websocket_ontick(ref); }, WS_TICK_INTERVAL );
	}


	// Function to actually send data to the websocket
	function __websocket_send(ref,data)
	{
		var websocket = datasources[ref];

		// Stringify the data
		var json = JSON.stringify(data);

		log_message("Sending: "+json);
		// Send it off
		websocket.handle.send(json);
	}


	// Function to register an ajax handler
	function _register_ajax(options,datasource)
	{
		// Check if datasource has a url
		if (typeof(datasource.url) === 'undefined') {
			log_message('Ajax datasource has no property "url"');
			return;
		}

		// This is our unique datasource, websocket name
		var ajaxname;
		if (typeof(datasource.shared) !== 'undefined' && datasource.shared) {
			ajaxname = datasource.url;
		} else {
			ajaxname = options.id;
		}

		// Get ajax handle
		var ajax = _ajax_connect(ajaxname,datasource.url);

		return ajaxname;
	}


	// Function to connect a ajax endpoint
	function _ajax_connect(ref,url)
	{
		// Check if ajax with this ref exists
		if (typeof(datasources[ref]) === 'object') {
			log_message("AJAX datasource already exists??");
			return;
		}

		// Load data from ajax
		var ajax = jQuery.ajax({
				url: url,
				dataType: 'json',
				success: function(message) { _ajax_onmessage(ref,message); },
				complete: function(request,textStatus) { _ajax_oncomplete(ref); }
		});

		// Inititalize datasources for this ref
		datasources[ref] = {
			handle: ajax,
			plots: { }
		};

	}


	// Handle AJAX data
	function _ajax_onmessage(ref,response)
	{
		var ajax = datasources[ref];

		log_message('AJAX Message: ');

		// Check result
		if (response.status == "success") {
			// Update plots with the data we just got
			_update_plots(ajax,response.data);

		} else if (response.status == "error") {
			log_message('AJAX Server Error: '+response.message);

		} else if (response.status == "fail") {
			log_message('AJAX Server Fail: '+response.message);
		}
	}


	// Function to handle coplete event
	function _ajax_oncomplete(ref)
	{
		var ajax = datasources[ref];

		log_message('AJAX Close: '+ref);

		// Remove ajax from list
		delete datasources[ref];
	}


	// Function to initialize a graphs data structures
	function _init_graph_data(pid)
	{
		// Initialize the plots data
		graphData[pid] = { };

		// Create blank datasets and the correct labels for all axis, tags, identifiers
		for (var axisIndex = 0; axisIndex < AXES.length; axisIndex++) {
			var axis = AXES[axisIndex];

			// Check if this axis is configured first of all
			if (typeof(plots[pid][axis]) === 'undefined') {
				continue;
			}
			// Inititalize axis
			graphData[pid][axis] = { };
			// Loop with plot tags
			for (var tag in plots[pid][axis]) {
				// Initialize the tag
				graphData[pid][axis][tag] = { };
				// Loop with each identifier
				for (var identifier in plots[pid][axis][tag]) {
					// Set label if its specified, or use the identifier as default if not
					var label;
					if (typeof(plots[pid][axis][tag][identifier].label) !== 'undefined') {
						label = plots[pid][axis][tag][identifier].label;
					} else {
						label = identifier;
					}
					// Check for max count
					var maxCount;
					if (typeof(plots[pid][axis][tag][identifier].maxCount) !== 'undefined') {
						maxCount = plots[pid][axis][tag][identifier].maxCount;
					} else {
						maxCount = options.defaultMaxCount;
					}
					// Check for max timespan
					var maxTimespan;
					if (typeof(plots[pid][axis][tag][identifier].maxTimespan) !== 'undefined') {
						maxTimespan = plots[pid][axis][tag][identifier].maxTimespan;
					} else {
						maxTimespan = options.defaultMaxTimespan;
					}
					// Initialize graph data for this specific item
					graphData[pid][axis][tag][identifier] = {
						label: label,
						maxCount: maxCount,
						maxTimespan: maxTimespan * 1000,
						data: [ ]
					};
					// Add on overriding of timestamp if we have it
					if (typeof(plots[pid][axis][tag][identifier].overrideTimestamp) !== 'undefined') {
						graphData[pid][axis][tag][identifier].overrideTimestamp =
								plots[pid][axis][tag][identifier].overrideTimestamp;
					}
					// XXX: Special treatment for yaxis
					if (axis == "yaxis") {
						graphData[pid][axis][tag][identifier].yaxis = 2;
					}
				}
			}
		}
	}


	// Process new stats and return the plots whose data was updated
	function _process_stats(datasource,stats)
	{
		// Merge two object data properties
		function _merge_graph_data(src,dst) {
			// The data is in the format of  [[timestamp,value],...]]  which is an array
			for (var item = 0; item < src.data.length; item++) {
				// Add to graph data
				dst.data.push(src.data[item]);
				// Check its not too big... if it did exceed pull off first item
				if (dst.maxCount && dst.maxCount > 0) {
					while (dst.data.length > dst.maxCount) {
						dst.data.shift();
					}
				}
				// Check that we have just enough entries for a time period if we ensuring that timespan
				if (dst.maxTimespan && dst.maxTimespan > 0) {
/* NK: XXX - Don't use timezone magic for now
					while (dst.data.length > 1 && dst.data[0][0] < timezoneNowMS - dst.maxTimespan) {
*/
					while (dst.data.length > 1 && dst.data[0][0] < nowMS - dst.maxTimespan) {
						dst.data.shift();
					}
				}
			}
		}

		// List of plots updated
		var ret = [ ];

/* NK: XXX - Don't use timezone magic for now
		// Get offset we are from UTC
		var timezoneOffset = ((new Date).getTimezoneOffset() * 60) * -1;
		// NOW , miliseconds with timezone offset since 1970
		var timezoneNowMS = (new Date).getTime() + (timezoneOffset * 1000);
*/
		var nowMS = (new Date).getTime();

		// Fix timestamps for all tags
		for (var tag in stats) {
			// Loop with properties (identifiers)
			for (var identifier in stats[tag]) {
				// The data is in the format of  [[timestamp,value],...]]  which is an array
				for (var item = 0; item < stats[tag][identifier].data.length; item++) {
					// Parse integer
					stats[tag][identifier].data[item][0] = parseInt(stats[tag][identifier].data[item][0]);
/* NK: XXX - Don't use timezone magic for now
					// Add timezone
					stats[tag][identifier].data[item][0] += timezoneOffset;
*/
					// Convert to javascript timestamp
					stats[tag][identifier].data[item][0] *= 1000;
				}
			}
		}

		// Loop with plot ID's associated with this datasource
		for (var pid in datasource.plots) {
			var plotConfig = plots[pid];
			var updated = 0;

			// Loop with both x and y axes
			for (var axisIndex = 0; axisIndex < AXES.length; axisIndex++) {
				// Set axis to the text value
				var axis = AXES[axisIndex];

				// If this axis is undefined, just continue
				if (typeof(plotConfig[axis]) === 'undefined') {
					continue;
				}
				// Loop with all tags for this axis
				for (var tag in plotConfig[axis]) {
					// If we don't have this tag in the data, skip
					if (typeof(stats[tag]) === 'undefined') {
						continue;
					}
					// Loop with identifiers
					for (var identifier in plotConfig[axis][tag]) {
						// If we don't have this identifier in the data, skip
						if (typeof(stats[tag][identifier]) === 'undefined') {
							continue;
						}
						// Merge in new data
						_merge_graph_data(stats[tag][identifier],graphData[pid][axis][tag][identifier]);
						// Bump updates
						updated++;
					}
				}
			}

			// Add to list of updated plots
			if (updated) {
				ret.push(pid);
			}
		}

		return ret;
	}


	// Return data that flot expects
	function _generate_plot_data(pid)
	{
		var ret = [ ];

		// Loop with both x and y axes
		var dataIndex = 0;
		for (var axisIndex = 0; axisIndex < AXES.length; axisIndex++) {
			// Set axis to the text value
			var axis = AXES[axisIndex];

			// If this axis is undefined, just continue
			if (typeof(graphData[pid][axis]) === 'undefined') {
				continue;
			}
			// Loop with all tags for this axis
			for (var tag in graphData[pid][axis]) {
				// Loop with all identifiers
				for (var identifier in graphData[pid][axis][tag]) {
					var retIndex = dataIndex++;

					// Create a blank record
					ret[retIndex] = {
						label: graphData[pid][axis][tag][identifier].label,
						data: [ ]
					};

					// Do we have any kind of overrides?
					// XXX: This may need work if we implement more transforms
					// XXX: We short circtui to a plain copy if we not changing data so we don't have to iterate
					if (typeof(graphData[pid][axis][tag][identifier].overrideTimestamp) !== 'undefined') {
						// Populate with data with possible data transforms
						for (var statIndex = 0; statIndex < graphData[pid][axis][tag][identifier].data.length; statIndex++) {
							ret[retIndex].data[statIndex] = graphData[pid][axis][tag][identifier].data[statIndex];
							ret[retIndex].data[statIndex][0] = graphData[pid][axis][tag][identifier].overrideTimestamp;
						}
					} else {
						ret[retIndex].data = graphData[pid][axis][tag][identifier].data;
					}

					// Special treatment for the yaxis again... we need to add the axis number
					if (typeof(graphData[pid][axis][tag][identifier].yaxis) !== 'undefined') {
						ret[retIndex].yaxis = graphData[pid][axis][tag][identifier].yaxis;
					}
				}
			}
		}

		return ret;
	}


	// Function responsible for updating plats and doing data updates to their graph data
	function _update_plots(datasource,data)
	{
		// Update our graph data, and save the list of updated plots
		var updatedPlots = _process_stats(datasource,data);

		// Loop with the updated plots
		for (var plotIndex = 0; plotIndex < updatedPlots.length; plotIndex++) {
			var pid = updatedPlots[plotIndex];
			var plot = plots[pid].handle;
			// Generate plot data
			var plotData = _generate_plot_data(pid);
			// Set data
			plot.setData(plotData);
			// XXX: This must probably move somewhere more suitable, we only need to call on init
			plot.setupGrid();
			// And finally draw
			plot.draw();
			// Remove loading image
			if (plots[pid].loading) {
				jQuery('#'+pid).removeClass('awitds-loader');
				plots[pid].loading = false;
			}
		}
	}


	$.plot.plugins.push({
		// Plugin version
		version: '1.2',
		// Configuration option name
		name: 'awitds',
		// Init function
		init: init,
		// Plugin options
		options: options
	});

})(jQuery);


// vim: ts=4
