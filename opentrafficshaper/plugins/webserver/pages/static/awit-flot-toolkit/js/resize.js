/*
 * AllWorldIT Flot Resizing Functionality
 * Copyright (c) 2014, AllWorldIT
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 *
 */



// Trigger resize on document ready
$(document).ready(function() {
	$('.poolCanvas').each( function(i, o){
		awit_flot_resize(o, 2, -300);
	});

	$('.dashboardCanvas').each( function(i, o){
		awit_flot_resize(o, 3, 42);
	});
})



// Trigger resize on window resize
$(window).resize(function() {
	$('.poolCanvas').each( function(i, o){
		awit_flot_resize(o, 2, -300);
	});

	$('.dashboardCanvas').each( function(i, o){
		awit_flot_resize(o, 3, 42);
	});
})



// Function to resize a flot graph
// splitter: how many graphs to draw
// padding: amount of pixels to increase graph width by
function awit_flot_resize(canvas, splitter, padding) {
	if (typeof(splitter) === 'undefined') {
		splitter = 1;
	}

	if (typeof(padding) === 'undefined') {
		padding = 0;
	}

	// Grab the ratio of height to width
	var canvasRatio = canvas.height / canvas.width;
	var windowRatio = window.innerHeight / window.innerWidth;
	var width;
	var height;

	// Work out the height and width for the flot graph in relation to the window
	if (windowRatio < canvasRatio) {
		height = window.innerHeight;
		width = height / canvasRatio;
	} else {
		width = window.innerWidth;
		height = width * canvasRatio;
	}

	// Work out the current graphs width based on the total amount of graphs to be displayed
	width = Math.floor(width / splitter) - padding;

	// Update the style
	canvas.style.width = width + 'px';
	canvas.style.height = height + 'px';
};



// vim: ts=4
