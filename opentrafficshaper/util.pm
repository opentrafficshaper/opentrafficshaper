# OpenTrafficShaper utility functions
# Copyright (C) 2007-2023, AllWorldIT
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


package opentrafficshaper::util;

use strict;
use warnings;


# Exporter stuff
require Exporter;
our (@ISA,@EXPORT,@EXPORT_OK);
@ISA = qw(Exporter);
@EXPORT = qw(
	isIPv46
	isIPv46CIDR
);
@EXPORT_OK = qw(
);



# Check if this is a valid IPv4 or IPv6 address
sub isIPv46
{
	my ($rawIP) = @_;

	my $ip = NetAddr::IP->new($rawIP);
	if (!defined($ip)) {
		return;
	}
	if ($ip->version == 4) {
		if ($ip->masklen != 32) {
			return;
		}
		return $ip->addr;
	} elsif ($ip->version == 6) {
		if ($ip->masklen != 128) {
			return;
		}
		return $ip->short;
	}
	return;
}



# Check if this is a valid IPv4 or IPv6 CIDR
sub isIPv46CIDR
{
	my ($rawIP) = @_;

	my $ip = NetAddr::IP->new($rawIP);
	if (!defined($ip)) {
		return;
	}
	if ($ip->version() == 4) {
		return $ip->network()->addr() . "/" . $ip->masklen();
	} elsif ($ip->version == 6) {
		return $ip->network()->short() . "/" . $ip->masklen();
	}
	return;
}



1;