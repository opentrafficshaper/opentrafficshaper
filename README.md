# Introduction


# Requirements


## POE

Debian-based: libpoe-perl

Arch-based: perl-poe


## Config::IniFiles

Debian-based: libconfig-inifiles-perl

Arch-based: perl-config-inifiles


## DateTime

Debian-based: libdatetime-perl

Arch-based: perl-datetime


## Digest::SHA

Debian-based: libdigest-sha-perl

Arch-based: perl-digest-sha


## JSON

Debian-based: libjson-perl

Arch-based: perl-json


## DBD::MySQL

Debian-based: libdbd-mysql-perl

Arch-based: perl-dbd-mysql


## HTML::Entities module

Debian-based: libhtml-parser-perl

Arch-based: perl-html-parser


## NetAddr::IP

Debian-based: libnetaddr-ip-perl

Arch-based: perl-netaddr-ip


## URI

Debian: liburi-perl

Arch-based: perl-uri



# Installing OpenTrafficShaper

```
git clone https://gitlab.devlabs.linuxassist.net/opentrafficshaper/opentrafficshaper.git
```



# System Configuration


## Shorewall

If you are using Shorewall, you will need to make a few config changes in '/etc/shorewall/config'.

Change config options...
```
CLEAR_TC=No
TC_ENABLED=No
PROVIDER_BITS=0
```


## Module Options

Add this to /etc/modprobe.d/opentrafficshaper-htb.conf...
```
options sch_htb htb_rate_est=1
```

Rebuild your initramfs (on Arch you can use the below command)...
```
mkinitcpio -P
```

This step requires a reboot.


## Temporary Directories

Create directories...
```
mkdir /var/lib/opentrafficshaper
```



# Statistics Setup

Install MariaDB and initialize with...
```
mysql -u ots -p otsdb < database/stats.sql
```



# Security

At present there is no security provided in terms of the management port 8088. It is suggested using something like a reverse proxy
to provide username/password access or restricting access to a fixed IP.

An example of the Nginx http section can be found in the contrib/ directory.



# Next steps

1. Edit the config file

2. You can now run opentrafficshaper by using ./opentrafficshaperd --debug --config=opentrafficshaper.conf --fg

3. Browse to it using http://SERVER:8080/


For more configuration information see this link:
http://wiki.opentrafficshaper.org/documentation

